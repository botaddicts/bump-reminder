import nextcord, datetime
from asyncio import sleep
from config import token, bump_message, bump_delay

import logging
logging.basicConfig(level=logging.INFO)

intents = nextcord.Intents.default()
intents.members = True
bot = nextcord.Client(intents=intents)

@bot.event
async def on_message(message):
    if str(message.author) == "DISBOARD#2760":
        print("New message from DISBOARD")
        if "Bump done" in message.embeds[0].description:
            print("New done message from DISBOARD")
            await sleep(bump_delay)
            await message.channel.send(bump_message)

bot.run(token)
