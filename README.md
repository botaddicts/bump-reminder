# 🔔 Bump reminder
![shiny image](https://disboard.org/images/bot-command-image-bump.png)

Simple bump reminder bot.

## Requirements
* Python3
* Linux system with systemd
* Discord bot (with intents for messages enabled)

## Installation

Clone the repository, go in it, install nextcord, copy the template config file, edit it and run the script.

```bash
git clone https://codeberg.org/botaddicts/bump-reminder.git
cd bump-reminder/
pip install -r requirements.txt
cp config.template.py config.py
nano config.py
python3 main.py
```

And for systemd: (running in the background):

```bash
sed -i "s|ABSOLUTE_PATH|$(pwd)|g" bump-reminder.service
sed -i "s|USER|$USER|g" bump-reminder.service
sudo cp bump-reminder.service /etc/systemd/system/
sudo systemctl start bump-reminder
```

## Usage
Bump the server once, then the bot will automatically start a count down and will remind people to bump 2 hours after the bump. 
